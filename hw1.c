#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Прокопенко Андрей, АПО-11, ИЗ N1
 * Задание A-4:
 * Составить программу разложения положительного целого числа на простые
 * сомножители и единицу.
 * Программа считывает входные данные со стандартного ввода, и печатает
 * результат в стандартный вывод.
 *
 * Программа посимвольно считвает из стандартного потока ввода строку 1-20
 * символов 0-9,
 * пропуская пробелы до и после, после чего переводится в число типа size_t.
 * Полученное
 * число обрабатывется - делим num на числа от 0 до sqrt(num) до тех пор пока
 * оно не разделится
 * на одно из них или до тех пор, пока не проверим весь ряд. Если разделилось,
 * то добавляем
 * делитель в динамический массив. По окончанию проверок, в конец массива
 * добавляем 0,
 * чтоб потом успешно его вывести.
 * Если число слишком большое (>2^63 - 1) или строка содержит сторонние символы,
 * а также при
 * неудачных выделениях памяти, выводится "[error]".
 */

const short NUMBUF = 20;
const short NUMOFNUM = 5;

void handle_error(void) { printf("[error]\n"); }

int allocate(size_t **ptr, int size) {
  size_t *nptr = (size_t *)realloc(*ptr, sizeof(size_t) * size);
  if (nptr == NULL) {
    free(*ptr);
    return 1;
  } else {
    *ptr = nptr;
  }
  return 0;
}

int check_buf(char *buf, int size) {
  int i = 0;
  while (buf[i] != '\0') {
    if (!(('0' <= buf[i] && buf[i] <= '9') || (buf[i] == ' '))) {
      return 1;
    }
    ++i;
  }
  return 0;
}

int input(size_t *num) {
  char *str = (char *)calloc(1, 1);
  if (!str) {
    free(str);
    return 1;
  }

  char buffer[NUMBUF];
  while (fgets(buffer, NUMBUF, stdin)) {
    char *ptrstr = (char *)realloc(str, strlen(str) + 1 + strlen(buffer));
    if (!ptrstr) {
      free(str);
      return 1;
    } else {
      str = ptrstr;
    }
    if (check_buf(buffer, NUMBUF)) {
      free(str);
      return 1;
    } else {
      strncat(str, buffer, NUMBUF);
    }
  }

  char *endptr = NULL;
  long long int mynum = strtoll(str, &endptr, 10);
  free(str);
  if (mynum == LLONG_MIN || mynum == LLONG_MAX || mynum <= 0) {
    return 1;
  } else {
    *num = (size_t)mynum;
  }

  return 0;
}

size_t *decay(size_t st_num) {
  size_t num = st_num;
  size_t *ptr = (size_t *)malloc(sizeof(size_t) * NUMOFNUM);
  if (ptr == NULL) {
    free(ptr);
    return NULL;
  }

  int i = 2;
  int k = 1;
  int count = 1;
  ptr[0] = 1;
  while (i * i <= num) {
    if (num % i == 0) {
      num /= i;
      ptr[count] = i;
      ++count;

      if (count == NUMOFNUM * k) {
        ++k;
        if (allocate(&ptr, NUMOFNUM * k)) {
          return NULL;
        }
      }

    } else {
      ++i;
    }
  }

  if (count < NUMBUF * k) {
    if (allocate(&ptr, count + 2)) {
      return NULL;
    }
  }

  if (st_num == 1) {
    ptr[count] = 0;
  } else if (st_num == num) {
    ptr[count] = st_num;
  } else {
    ptr[count] = num;
  }

  ptr[++count] = 0;

  return ptr;
}

int main(void) {
  size_t num = 0;
  if (input(&num)) {
    handle_error();
    return 0;
  }

  size_t *array = decay(num);
  if (array == NULL) {
    handle_error();
    return 0;
  }

  int i = 0;
  while (array[i] != 0) {
    printf("%zu ", array[i]);
    i++;
  }

  putchar('\n');

  free(array);
  return 0;
}

