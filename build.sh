#!/bin/bash

case "$1" in
	"main.out")
	`gcc -std=c99 -o main.out hw1.c -lm`;;
	"check")
	`valgrind --tool=memcheck --leak-check=full ./main.out`;;
esac

